# Wordpress Plugin

If you have a Wordpress site and want to install EPICA pixel with EPICA Wordpress Plugin follow these steps:

In your Wordpress Admin Page Menu, go to Plugins>Add New

1. Select “Upload Plugin” besides “Add Plugins”
2. Select “Search Files” and select Epica Wordpress Plugin zip file, then select “Install Now”
3. Now you have successfully installed Epica Plugin, you can activate it right away clicking on “Activate Plugin” button or go to Plugins> Installed Plugins to activate / deactivate it wherever you like.
4. Here, you can also go to “Settings”
5. You must include the “EPICA Write Key” that your Client Partner in EPICA gave you in “EPICA API Key” text box.
6. You can also change the settings to adjust EPICA Tracking in your website.
7. “Save Changes” when finished!

Congratulations! Now you are tracking Events in your website using EPICA Wordpress Plugin.